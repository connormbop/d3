﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrivingSchoolD3.controller;

namespace DrivingSchoolD3
{
    public partial class manageAppointments : Form
    {
        public string query = "SELECT DISTINCT DATES FROM INSTRUCTORAVAIL";
        public string query2 = $"";
        public string query3 = "";
        public string date;
        public string time;
        public manageAppointments()
        {
            InitializeComponent();
            SQL.editComboBoxItems(dateCBox, query);


        }

        private void setBtn_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"UPDATE TIMESLOTFINAL SET LICENSE = '{licenseCBox.Text}' WHERE TIMES = '{timeCBox.Text}' AND DATES = '{dateCBox.Text}'");
            timeCBox.Text = "";
            dateCBox.Text = "";
            licenseCBox.Text = "";
        }

        private void timeCBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void refresh_btn_Click(object sender, EventArgs e)
        {
            date = dateCBox.Text;
            time = timeCBox.Text;



            if (timeCBox.Text == "")
            {
                query2 = $"SELECT DISTINCT TIMES FROM TIMEPLUSDATES WHERE DATES = '{date}'";
                SQL.editComboBoxItems(timeCBox, query2);
            }
            else
            {
                query3 = $"SELECT LICENSE FROM VEHICLEBOOKING WHERE TIMES = '{time}' AND DATES = '{date}'";
                SQL.editComboBoxItems(licenseCBox, query3);
            }
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }
    }
}
