﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrivingSchoolD3.model;

namespace DrivingSchoolD3.controller
{
    class ClientQueries
    {
        public static List<Client> DisplayAllClientsInList()
        {
            var clients = new List<Client>();
            var dr = SQL.selectQuery("Select * From CLIENT");

            while (dr.Read())
            {
                if (dr.HasRows)
                {
                    clients.Add(new Client { CUSERNAME = $"{dr["CUSERNAME"]}", CPASSWORD = $"{dr["CPASSWORD"]}", CSNAME = $"{dr["CSNAME"]}", CFNAME = $"{dr["CFNAME"]}", CEMAIL = $"{dr["CEMAIL"]}", CPHONE = $"{dr["CPHONE"]}", CEXP = $"{dr["CEXP"]}" });
                }
            }

            return clients;
        }
        public static int InsertNewData(string _cUN, string _cPW, string _cSN, string _cFN, string _cEM, string _cPH, string _cEX)
        {
            var query = $"INSERT INTO CLIENT VALUES ('{_cUN}','{_cPW}','{_cSN}','{_cFN}','{_cEM}','{_cPH}','{_cEX}')";
            return SQL.executeQuery(query);
        }
    }
}
