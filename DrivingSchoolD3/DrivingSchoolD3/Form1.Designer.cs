﻿namespace DrivingSchoolD3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pBox = new System.Windows.Forms.TextBox();
            this.uBox = new System.Windows.Forms.TextBox();
            this.loginBtn = new System.Windows.Forms.Button();
            this.cRegoBtn = new System.Windows.Forms.Button();
            this.Staff_Rego_Btn = new System.Windows.Forms.Button();
            this.typeCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Username";
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(12, 78);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(92, 20);
            this.pBox.TabIndex = 13;
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(12, 25);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(92, 20);
            this.uBox.TabIndex = 12;
            // 
            // loginBtn
            // 
            this.loginBtn.Location = new System.Drawing.Point(110, 46);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(162, 52);
            this.loginBtn.TabIndex = 16;
            this.loginBtn.Text = "Login";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // cRegoBtn
            // 
            this.cRegoBtn.Location = new System.Drawing.Point(12, 201);
            this.cRegoBtn.Name = "cRegoBtn";
            this.cRegoBtn.Size = new System.Drawing.Size(130, 48);
            this.cRegoBtn.TabIndex = 17;
            this.cRegoBtn.Text = "Client Registration";
            this.cRegoBtn.UseVisualStyleBackColor = true;
            this.cRegoBtn.Click += new System.EventHandler(this.cRegoBtn_Click);
            // 
            // Staff_Rego_Btn
            // 
            this.Staff_Rego_Btn.Location = new System.Drawing.Point(146, 201);
            this.Staff_Rego_Btn.Name = "Staff_Rego_Btn";
            this.Staff_Rego_Btn.Size = new System.Drawing.Size(130, 48);
            this.Staff_Rego_Btn.TabIndex = 18;
            this.Staff_Rego_Btn.Text = "Staff Registration";
            this.Staff_Rego_Btn.UseVisualStyleBackColor = true;
            this.Staff_Rego_Btn.Click += new System.EventHandler(this.Staff_Rego_Btn_Click);
            // 
            // typeCombo
            // 
            this.typeCombo.FormattingEnabled = true;
            this.typeCombo.Items.AddRange(new object[] {
            "Client",
            "Administrator",
            "Instructor"});
            this.typeCombo.Location = new System.Drawing.Point(111, 25);
            this.typeCombo.Name = "typeCombo";
            this.typeCombo.Size = new System.Drawing.Size(161, 21);
            this.typeCombo.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "What are you?";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.typeCombo);
            this.Controls.Add(this.Staff_Rego_Btn);
            this.Controls.Add(this.cRegoBtn);
            this.Controls.Add(this.loginBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Name = "Form1";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox uBox;
        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.Button cRegoBtn;
        private System.Windows.Forms.Button Staff_Rego_Btn;
        private System.Windows.Forms.ComboBox typeCombo;
        private System.Windows.Forms.Label label3;
    }
}

