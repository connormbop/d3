﻿namespace DrivingSchoolD3
{
    partial class AdminHP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fNameLabel = new System.Windows.Forms.Label();
            this.InstructBookingBtn = new System.Windows.Forms.Button();
            this.VehicleBookingBtn = new System.Windows.Forms.Button();
            this.mAppointmentsBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.displayData = new System.Windows.Forms.ListBox();
            this.showAppointmentsBtn = new System.Windows.Forms.Button();
            this.displayDataTV = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // fNameLabel
            // 
            this.fNameLabel.AutoSize = true;
            this.fNameLabel.Location = new System.Drawing.Point(29, 13);
            this.fNameLabel.Name = "fNameLabel";
            this.fNameLabel.Size = new System.Drawing.Size(0, 13);
            this.fNameLabel.TabIndex = 0;
            // 
            // InstructBookingBtn
            // 
            this.InstructBookingBtn.Location = new System.Drawing.Point(32, 28);
            this.InstructBookingBtn.Name = "InstructBookingBtn";
            this.InstructBookingBtn.Size = new System.Drawing.Size(123, 53);
            this.InstructBookingBtn.TabIndex = 1;
            this.InstructBookingBtn.Text = "1: Instructor Availability";
            this.InstructBookingBtn.UseVisualStyleBackColor = true;
            this.InstructBookingBtn.Click += new System.EventHandler(this.InstructBookingBtn_Click);
            // 
            // VehicleBookingBtn
            // 
            this.VehicleBookingBtn.Location = new System.Drawing.Point(732, 28);
            this.VehicleBookingBtn.Name = "VehicleBookingBtn";
            this.VehicleBookingBtn.Size = new System.Drawing.Size(123, 53);
            this.VehicleBookingBtn.TabIndex = 2;
            this.VehicleBookingBtn.Text = "2: Vehicle Availability";
            this.VehicleBookingBtn.UseVisualStyleBackColor = true;
            this.VehicleBookingBtn.Click += new System.EventHandler(this.VehicleBookingBtn_Click);
            // 
            // mAppointmentsBtn
            // 
            this.mAppointmentsBtn.Location = new System.Drawing.Point(352, 29);
            this.mAppointmentsBtn.Name = "mAppointmentsBtn";
            this.mAppointmentsBtn.Size = new System.Drawing.Size(170, 52);
            this.mAppointmentsBtn.TabIndex = 3;
            this.mAppointmentsBtn.Text = "3: Manage Appointments";
            this.mAppointmentsBtn.UseVisualStyleBackColor = true;
            this.mAppointmentsBtn.Click += new System.EventHandler(this.mAppointmentsBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Load Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // displayData
            // 
            this.displayData.FormattingEnabled = true;
            this.displayData.Location = new System.Drawing.Point(12, 177);
            this.displayData.Name = "displayData";
            this.displayData.Size = new System.Drawing.Size(843, 173);
            this.displayData.TabIndex = 5;
            // 
            // showAppointmentsBtn
            // 
            this.showAppointmentsBtn.Location = new System.Drawing.Point(685, 148);
            this.showAppointmentsBtn.Name = "showAppointmentsBtn";
            this.showAppointmentsBtn.Size = new System.Drawing.Size(170, 23);
            this.showAppointmentsBtn.TabIndex = 6;
            this.showAppointmentsBtn.Text = "Show Details";
            this.showAppointmentsBtn.UseVisualStyleBackColor = true;
            this.showAppointmentsBtn.Click += new System.EventHandler(this.showAppointmentsBtn_Click);
            // 
            // displayDataTV
            // 
            this.displayDataTV.Location = new System.Drawing.Point(12, 356);
            this.displayDataTV.Name = "displayDataTV";
            this.displayDataTV.Size = new System.Drawing.Size(453, 432);
            this.displayDataTV.TabIndex = 7;
            // 
            // AdminHP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 800);
            this.Controls.Add(this.displayDataTV);
            this.Controls.Add(this.showAppointmentsBtn);
            this.Controls.Add(this.displayData);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.mAppointmentsBtn);
            this.Controls.Add(this.VehicleBookingBtn);
            this.Controls.Add(this.InstructBookingBtn);
            this.Controls.Add(this.fNameLabel);
            this.Name = "AdminHP";
            this.Text = "AdminHP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fNameLabel;
        private System.Windows.Forms.Button InstructBookingBtn;
        private System.Windows.Forms.Button VehicleBookingBtn;
        private System.Windows.Forms.Button mAppointmentsBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox displayData;
        private System.Windows.Forms.Button showAppointmentsBtn;
        private System.Windows.Forms.TreeView displayDataTV;
    }
}