﻿namespace DrivingSchoolD3
{
    partial class manageAppointments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.licenseCBox = new System.Windows.Forms.ComboBox();
            this.dateCBox = new System.Windows.Forms.ComboBox();
            this.timeCBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.setBtn = new System.Windows.Forms.Button();
            this.refresh_btn = new System.Windows.Forms.Button();
            this.backBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // licenseCBox
            // 
            this.licenseCBox.FormattingEnabled = true;
            this.licenseCBox.Location = new System.Drawing.Point(112, 128);
            this.licenseCBox.Name = "licenseCBox";
            this.licenseCBox.Size = new System.Drawing.Size(121, 21);
            this.licenseCBox.TabIndex = 0;
            // 
            // dateCBox
            // 
            this.dateCBox.FormattingEnabled = true;
            this.dateCBox.Location = new System.Drawing.Point(112, 35);
            this.dateCBox.Name = "dateCBox";
            this.dateCBox.Size = new System.Drawing.Size(121, 21);
            this.dateCBox.TabIndex = 1;
            // 
            // timeCBox
            // 
            this.timeCBox.FormattingEnabled = true;
            this.timeCBox.Location = new System.Drawing.Point(112, 81);
            this.timeCBox.Name = "timeCBox";
            this.timeCBox.Size = new System.Drawing.Size(121, 21);
            this.timeCBox.TabIndex = 2;
            this.timeCBox.SelectedIndexChanged += new System.EventHandler(this.timeCBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "License";
            // 
            // setBtn
            // 
            this.setBtn.Location = new System.Drawing.Point(139, 189);
            this.setBtn.Name = "setBtn";
            this.setBtn.Size = new System.Drawing.Size(94, 23);
            this.setBtn.TabIndex = 6;
            this.setBtn.Text = "Set";
            this.setBtn.UseVisualStyleBackColor = true;
            this.setBtn.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // refresh_btn
            // 
            this.refresh_btn.Location = new System.Drawing.Point(16, 189);
            this.refresh_btn.Name = "refresh_btn";
            this.refresh_btn.Size = new System.Drawing.Size(94, 23);
            this.refresh_btn.TabIndex = 7;
            this.refresh_btn.Text = "Refresh";
            this.refresh_btn.UseVisualStyleBackColor = true;
            this.refresh_btn.Click += new System.EventHandler(this.refresh_btn_Click);
            // 
            // backBtn
            // 
            this.backBtn.Location = new System.Drawing.Point(82, 226);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(94, 23);
            this.backBtn.TabIndex = 8;
            this.backBtn.Text = "Back To Login";
            this.backBtn.UseVisualStyleBackColor = true;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // manageAppointments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.refresh_btn);
            this.Controls.Add(this.setBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.timeCBox);
            this.Controls.Add(this.dateCBox);
            this.Controls.Add(this.licenseCBox);
            this.Name = "manageAppointments";
            this.Text = "manageAppointments";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox licenseCBox;
        private System.Windows.Forms.ComboBox dateCBox;
        private System.Windows.Forms.ComboBox timeCBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button setBtn;
        private System.Windows.Forms.Button refresh_btn;
        private System.Windows.Forms.Button backBtn;
    }
}