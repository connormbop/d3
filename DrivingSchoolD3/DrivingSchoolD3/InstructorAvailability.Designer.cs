﻿namespace DrivingSchoolD3
{
    partial class InstructorAvailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.instructorCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timeCombo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.setBtn = new System.Windows.Forms.Button();
            this.toLogin = new System.Windows.Forms.Button();
            this.refresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(83, 74);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(188, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // instructorCombo
            // 
            this.instructorCombo.FormattingEnabled = true;
            this.instructorCombo.Location = new System.Drawing.Point(83, 35);
            this.instructorCombo.Name = "instructorCombo";
            this.instructorCombo.Size = new System.Drawing.Size(188, 21);
            this.instructorCombo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(137, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pick an Instructor";
            // 
            // timeCombo
            // 
            this.timeCombo.FormattingEnabled = true;
            this.timeCombo.Items.AddRange(new object[] {
            "9:00am - 10:00am",
            "10:00am - 11.00am",
            "11:00am - 12:00pm",
            "12:00pm - 1:00pm",
            "1:00pm - 2:00pm",
            "2:00pm - 3:00pm",
            "3:00pm - 4:00pm",
            "4:00pm - 5:00pm"});
            this.timeCombo.Location = new System.Drawing.Point(83, 121);
            this.timeCombo.Name = "timeCombo";
            this.timeCombo.Size = new System.Drawing.Size(188, 21);
            this.timeCombo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Time";
            // 
            // setBtn
            // 
            this.setBtn.Location = new System.Drawing.Point(205, 188);
            this.setBtn.Name = "setBtn";
            this.setBtn.Size = new System.Drawing.Size(66, 23);
            this.setBtn.TabIndex = 5;
            this.setBtn.Text = "Set";
            this.setBtn.UseVisualStyleBackColor = true;
            this.setBtn.Click += new System.EventHandler(this.setBtn_Click);
            // 
            // toLogin
            // 
            this.toLogin.Location = new System.Drawing.Point(83, 188);
            this.toLogin.Name = "toLogin";
            this.toLogin.Size = new System.Drawing.Size(111, 23);
            this.toLogin.TabIndex = 6;
            this.toLogin.Text = "Back to Login";
            this.toLogin.UseVisualStyleBackColor = true;
            this.toLogin.Click += new System.EventHandler(this.toLogin_Click);
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(205, 155);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(66, 23);
            this.refresh.TabIndex = 7;
            this.refresh.Text = "Refresh";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // InstructorAvailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 268);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.toLogin);
            this.Controls.Add(this.setBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.timeCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.instructorCombo);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "InstructorAvailability";
            this.Text = "InstructorAvailability";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox instructorCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox timeCombo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button setBtn;
        private System.Windows.Forms.Button toLogin;
        private System.Windows.Forms.Button refresh;
    }
}