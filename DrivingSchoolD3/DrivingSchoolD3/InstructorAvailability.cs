﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DrivingSchoolD3.controller;

namespace DrivingSchoolD3
{
    public partial class InstructorAvailability : Form
    {
        public string[] instruct;
        public int i;
        public string adminQ;
        public string query = "";
        public string query2 = "";
        public string query3 = "";
        public string date;
        public string instructor;
        public int numberofEntries;

        public InstructorAvailability(string fname)
        {
            InitializeComponent();
            query = $"SELECT DISTINCT IFNAME FROM INSTRUCTOR WHERE IUSERNAME = '{fname}'";
            SQL.editComboBoxItems(instructorCombo, query);
            //SQL.editComboBoxItems(timeCombo, query2);
        }

        private void setBtn_Click(object sender, EventArgs e)
        {

            //MessageBox.Show($"time:{timeCombo.Text}, date:{dateTimePicker1.Text}");
            SQL.executeQuery($"INSERT INTO TIMEPLUSDATES(TIMES,DATES) VALUES ('{timeCombo.Text}', '{dateTimePicker1.Text}')");
            SQL.executeQuery($"INSERT INTO INSTRUCTORAVAIL(IUSERNAME, IFNAME,TIMES,DATES) VALUES((SELECT IUSERNAME FROM INSTRUCTOR WHERE IFNAME = '{instructorCombo.Text}'), '{instructorCombo.Text}','{timeCombo.Text}','{dateTimePicker1.Text}')");
        }

        private void toLogin_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            //date = dateTimePicker1.Text;
            //query2 = $"SELECT TIMES FROM TIMEPLUSDATES WHERE DATES = '{date}'";


            //date = dateTimePicker1.Text;
            //if (instructorCombo.Text == "")
            //{

            //}
            //else
            //{
            //    query2 = $"SELECT TIMES FROM TIMEPLUSDATES WHERE DATES = '{date}'";
            //    SQL.editComboBoxItems(timeCombo, query2);
            //}
        }
    }
}
