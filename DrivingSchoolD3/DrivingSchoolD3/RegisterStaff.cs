﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrivingSchoolD3.controller;

namespace DrivingSchoolD3
{
    public partial class RegisterStaff : Form
    {
        public RegisterStaff()
        {
            InitializeComponent();
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {
            if(staffPosBox.Text == "Instructor")
            {
                StaffQueries.InsertNewDataInstructor(uBox.Text, pBox.Text, sNBox.Text, fNBox.Text, emailBox.Text, phoneBox.Text);
            }
            else if (staffPosBox.Text == "Administrator")
            {
                StaffQueries.InsertNewDataAdmin(uBox.Text, pBox.Text, sNBox.Text, fNBox.Text, emailBox.Text, phoneBox.Text);
            }
            else
            {
                MessageBox.Show("Please Pick Your Staff Position");
            }
        }

        private void bkBtn_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }
    }
}
