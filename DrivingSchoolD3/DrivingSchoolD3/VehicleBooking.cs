﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DrivingSchoolD3.setup;
using DrivingSchoolD3.controller;

namespace DrivingSchoolD3
{
    public partial class VehicleBooking : Form
    {
        public string query = "SELECT LICENSE FROM CAR";
        public string dateQuery = "SELECT DISTINCT DATES FROM INSTRUCTORAVAIL";
        public string timeQuery = "SELECT DISTINCT TIMES FROM INSTRUCTORAVAIL";
        public VehicleBooking()
        {
            InitializeComponent();

            SQL.editComboBoxItems(vehicles, query);

            SQL.editComboBoxItems(appointmentBox, dateQuery);
            SQL.editComboBoxItems(timebox, timeQuery);

        }

        void cars()
        {
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {

            

            //cars();

        }

        private void vehicleSetBtn_Click(object sender, EventArgs e)
        {
            //time = "SELECT TIMES FROM INSTRUCTORAVAIL";
            SQL.executeQuery($"INSERT INTO VEHICLEBOOKING(LICENSE, TIMES, DATES) VALUES('{vehicles.Text}', '{timebox.Text}', '{appointmentBox.Text}')"); 
            

            //else if (vehicles.Text == "123ABC")
            //{
            //    makeLabel.Text = "TOYOTA";
            //}
            //else if (vehicles.Text == "456DEF")
            //{
            //    makeLabel.Text = "MAZDA";
            //}
            //else if (vehicles.Text == "789GHI")
            //{
            //    makeLabel.Text = "FORD";
            //}
            //else if (vehicles.Text == "065JKL")
            //{
            //    makeLabel.Text = "HOLDEN";
            //}
            //else
            //{
            //    MessageBox.Show("Please pick a license plate for the car");
            //}
            
        }

        private void vehicles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (vehicles.Text == "FAST05")
            {
                makeLabel.Text = "FERRARI";
            }
            else if (vehicles.Text == "123ABC")
            {
                makeLabel.Text = "TOYOTA";
            }
            else if (vehicles.Text == "456DEF")
            {
                makeLabel.Text = "MAZDA";
            }
            else if (vehicles.Text == "789GHI")
            {
                makeLabel.Text = "FORD";
            }
            else if (vehicles.Text == "065JKL")
            {
                makeLabel.Text = "HOLDEN";
            }
            else
            {
                MessageBox.Show("Please pick a license plate for the car");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }
    }
}
