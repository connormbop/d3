﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrivingSchoolD3.model
{
    class Admin
    {
        public string AUSERNAME { get; set; }
        public string APASSWORD { get; set; }
        public string ASNAME { get; set; }
        public string AFNAME { get; set; }
        public string AEMAIL { get; set; }
        public string APHONE { get; set; }
    }
}
