﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrivingSchoolD3.model
{
    class Client
    {
        public string CUSERNAME { get; set; }
        public string CPASSWORD { get; set; }
        public string CSNAME { get; set; }
        public string CFNAME { get; set; }
        public string CEMAIL { get; set; }
        public string CPHONE { get; set; }
        public string CEXP { get; set; }

    }
}
