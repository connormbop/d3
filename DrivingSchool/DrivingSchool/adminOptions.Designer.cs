﻿namespace DrivingSchool
{
    partial class adminOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iAvail = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.vAvail = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // iAvail
            // 
            this.iAvail.Location = new System.Drawing.Point(15, 35);
            this.iAvail.Name = "iAvail";
            this.iAvail.Size = new System.Drawing.Size(100, 23);
            this.iAvail.TabIndex = 0;
            this.iAvail.Text = "Check";
            this.iAvail.UseVisualStyleBackColor = true;
            this.iAvail.Click += new System.EventHandler(this.iAvail_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Instructor Availability";
            // 
            // vAvail
            // 
            this.vAvail.Location = new System.Drawing.Point(139, 34);
            this.vAvail.Name = "vAvail";
            this.vAvail.Size = new System.Drawing.Size(91, 23);
            this.vAvail.TabIndex = 2;
            this.vAvail.Text = "Check";
            this.vAvail.UseVisualStyleBackColor = true;
            this.vAvail.Click += new System.EventHandler(this.vAvail_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(136, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Vehicle Availability";
            // 
            // adminOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 498);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.vAvail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.iAvail);
            this.Name = "adminOptions";
            this.Text = "adminOptions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button iAvail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button vAvail;
        private System.Windows.Forms.Label label2;
    }
}