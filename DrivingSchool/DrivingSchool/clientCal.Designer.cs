﻿namespace DrivingSchool
{
    partial class clientCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.booked = new System.Windows.Forms.ListView();
            this.clientCalPicker = new System.Windows.Forms.DateTimePicker();
            this.bookBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // booked
            // 
            this.booked.Location = new System.Drawing.Point(102, 190);
            this.booked.Name = "booked";
            this.booked.Size = new System.Drawing.Size(324, 127);
            this.booked.TabIndex = 8;
            this.booked.UseCompatibleStateImageBehavior = false;
            this.booked.SelectedIndexChanged += new System.EventHandler(this.booked_SelectedIndexChanged);
            // 
            // clientCalPicker
            // 
            this.clientCalPicker.Location = new System.Drawing.Point(92, 40);
            this.clientCalPicker.Name = "clientCalPicker";
            this.clientCalPicker.Size = new System.Drawing.Size(335, 20);
            this.clientCalPicker.TabIndex = 7;
            this.clientCalPicker.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // bookBtn
            // 
            this.bookBtn.Location = new System.Drawing.Point(223, 352);
            this.bookBtn.Name = "bookBtn";
            this.bookBtn.Size = new System.Drawing.Size(84, 29);
            this.bookBtn.TabIndex = 6;
            this.bookBtn.Text = "Book";
            this.bookBtn.UseVisualStyleBackColor = true;
            this.bookBtn.Click += new System.EventHandler(this.bookBtn_Click);
            // 
            // clientCal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 491);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.booked);
            this.Controls.Add(this.clientCalPicker);
            this.Controls.Add(this.bookBtn);
            this.Name = "clientCal";
            this.Text = "clientCal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView booked;
        private System.Windows.Forms.DateTimePicker clientCalPicker;
        private System.Windows.Forms.Button bookBtn;
    }
}