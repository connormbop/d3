﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DrivingSchool
{
    public partial class instructSchedule : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source = BCS303A-04A; Database = DRIVINGSCHOOL; Integrated Security = True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        public string user;
        public string day;
        public string time;
        public instructSchedule()
        {
            InitializeComponent();
            timeBox.Items.Add("08:00");
            timeBox.Items.Add("08:30");
            timeBox.Items.Add("09:00");
            timeBox.Items.Add("09:30");
            timeBox.Items.Add("10:00");
            timeBox.Items.Add("10:30");
            timeBox.Items.Add("11:00");
            timeBox.Items.Add("11:30");
            timeBox.Items.Add("12:00");
            timeBox.Items.Add("12:30");
            timeBox.Items.Add("13:00");
            timeBox.Items.Add("13:30");
            timeBox.Items.Add("14:00");
            timeBox.Items.Add("14:30");
            timeBox.Items.Add("15:00");
            timeBox.Items.Add("15:30");
            timeBox.Items.Add("16:00");
            timeBox.Items.Add("16:30");
        }

        private void bookBtn_Click(object sender, EventArgs e)
        {
            try
            {
                user = iUser.Text;
                day = iCalPicker.Text;
                time = timeBox.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            try
            {
                SQL.executeQuery("INSERT INTO TIMEINSTRUCT(NAME, DAY, TIME) VALUES ('" + user + "', '" + day + "', '" + time + "')");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
            MessageBox.Show("Successfully Registered: " + user + " You have been booked for " + day + ". Your timeslot is " + time);
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {


        }
        void refreshQuery()
        {
            


        }

        void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS303A-04A;Database=DRIVINGSCHOOL;Integrated Security=True"))
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT * FROM TIMEINSTRUCT", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    booked.Items.Add(sqlReader["NAME"].ToString() + " " + (sqlReader["DAY"].ToString() + " " + (sqlReader["TIME"].ToString())));
                }
                sqlReader.Close();
            }
            con.Open();
            cmd.CommandText = "select * from TIMEINSTRUCT";
            cmd.Connection = con;
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                booked.View = View.Details;

                ListViewItem item = new ListViewItem(dr["NAME"].ToString());
                item.SubItems.Add(dr["DAY"].ToString());
                item.SubItems.Add(dr["TIME"].ToString());
                booked.Items.Add(item);
            }
            con.Close();
        }
    }
}

