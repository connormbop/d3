﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DrivingSchool
{
    public partial class ClientReg : Form
    {
        public string username;
        public string password;
        public string sname;
        public string fname;
        public string email;
        public string phone;
        public string exp;

        public ClientReg()
        {
            InitializeComponent();
        }

        private void enterBtn_Click(object sender, EventArgs e)
        {

            string username = "", password = "", sname = "", fname = "", email = "", phone = "" , cexp = "";

            try
            {
                username = uBox.Text.Trim();
                password = pBox.Text.Trim();
                fname = fNBox.Text.Trim();
                sname = sNBox.Text.Trim();
                email = emailBox.Text.Trim();
                phone = phoneBox.Text.Trim();
                cexp = eLevelCBox.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            try
            {
                SQL.executeQuery("INSERT INTO CLIENT VALUES ('" + username + "', '" + password + "', '" + sname + "', '" + fname + "', '" + email + "', '" + phone + "', '" + cexp + "' )");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
            MessageBox.Show("Successfully Registered: " + fname + " " + sname + ". Your username is: " + username);
        }

        private void bkBtn_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }
    }
}
