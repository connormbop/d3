﻿namespace DrivingSchool
{
    partial class InstructReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.sNBox = new System.Windows.Forms.TextBox();
            this.fNBox = new System.Windows.Forms.TextBox();
            this.emailBox = new System.Windows.Forms.TextBox();
            this.phoneBox = new System.Windows.Forms.TextBox();
            this.enterBtn = new System.Windows.Forms.Button();
            this.bkBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(172, 13);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(100, 20);
            this.uBox.TabIndex = 0;
            this.uBox.TextChanged += new System.EventHandler(this.uBox_TextChanged);
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(172, 48);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(100, 20);
            this.pBox.TabIndex = 1;
            this.pBox.TextChanged += new System.EventHandler(this.pBox_TextChanged);
            // 
            // sNBox
            // 
            this.sNBox.Location = new System.Drawing.Point(172, 84);
            this.sNBox.Name = "sNBox";
            this.sNBox.Size = new System.Drawing.Size(100, 20);
            this.sNBox.TabIndex = 2;
            this.sNBox.TextChanged += new System.EventHandler(this.sNBox_TextChanged);
            // 
            // fNBox
            // 
            this.fNBox.Location = new System.Drawing.Point(172, 119);
            this.fNBox.Name = "fNBox";
            this.fNBox.Size = new System.Drawing.Size(100, 20);
            this.fNBox.TabIndex = 3;
            this.fNBox.TextChanged += new System.EventHandler(this.fNBox_TextChanged);
            // 
            // emailBox
            // 
            this.emailBox.Location = new System.Drawing.Point(172, 154);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(100, 20);
            this.emailBox.TabIndex = 4;
            this.emailBox.TextChanged += new System.EventHandler(this.emailBox_TextChanged);
            // 
            // phoneBox
            // 
            this.phoneBox.Location = new System.Drawing.Point(172, 190);
            this.phoneBox.Name = "phoneBox";
            this.phoneBox.Size = new System.Drawing.Size(100, 20);
            this.phoneBox.TabIndex = 5;
            this.phoneBox.TextChanged += new System.EventHandler(this.phoneBox_TextChanged);
            // 
            // enterBtn
            // 
            this.enterBtn.Location = new System.Drawing.Point(172, 226);
            this.enterBtn.Name = "enterBtn";
            this.enterBtn.Size = new System.Drawing.Size(100, 23);
            this.enterBtn.TabIndex = 6;
            this.enterBtn.Text = "Enter";
            this.enterBtn.UseVisualStyleBackColor = true;
            this.enterBtn.Click += new System.EventHandler(this.enterBtn_Click);
            // 
            // bkBtn
            // 
            this.bkBtn.Location = new System.Drawing.Point(13, 226);
            this.bkBtn.Name = "bkBtn";
            this.bkBtn.Size = new System.Drawing.Size(75, 23);
            this.bkBtn.TabIndex = 7;
            this.bkBtn.Text = "Back";
            this.bkBtn.UseVisualStyleBackColor = true;
            this.bkBtn.Click += new System.EventHandler(this.bkBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Username:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Password:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Surname:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "First Name";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Email:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Phone:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // InstructReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bkBtn);
            this.Controls.Add(this.enterBtn);
            this.Controls.Add(this.phoneBox);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.fNBox);
            this.Controls.Add(this.sNBox);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Name = "InstructReg";
            this.Text = "Instructor Registration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox uBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox sNBox;
        private System.Windows.Forms.TextBox fNBox;
        private System.Windows.Forms.TextBox emailBox;
        private System.Windows.Forms.TextBox phoneBox;
        private System.Windows.Forms.Button enterBtn;
        private System.Windows.Forms.Button bkBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}