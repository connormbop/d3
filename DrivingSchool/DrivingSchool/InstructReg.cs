﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrivingSchool
{ 
    public partial class InstructReg : Form
    {
        public string username;
        public string password;
        public string sname;
        public string fname;
        public string email;
        public string phone;
        public InstructReg()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void enterBtn_Click(object sender, EventArgs e)
        {
            string username = "", password = "", sname = "", fname = "", email = "", phone = "";

            try
            {
                username = uBox.Text.Trim();
                password = pBox.Text.Trim();
                fname = fNBox.Text.Trim();
                sname = sNBox.Text.Trim();
                email = emailBox.Text.Trim();
                phone = phoneBox.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            try
            {
                SQL.executeQuery("INSERT INTO INSTRUCTOR VALUES ('" + username + "', '" + password + "', '" + sname + "', '" + fname + "', '" + email + "', '" + phone + "' )");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
            MessageBox.Show("Successfully Registered: " + fname + " " + sname + ". Your username is: " + username);
        }

        private void bkBtn_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void phoneBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void emailBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void fNBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void sNBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void pBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void uBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

