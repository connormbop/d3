﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DrivingSchool
{
    public partial class Form1 : Form
    {
        public string username;
        public string password;


        public Form1()
        {
            InitializeComponent();
        }
        private void loginBtn_Click(object sender, EventArgs e)
        {
            if (clientCHK.Checked == true)
            {
                bool loggedIn = false;
                string username = "", password = "", fname = "", sname = "";

                //check if boxes are empty, the Trim removes white space in text from either side
                if ("".Equals(uBox.Text.Trim()) || "".Equals(pBox.Text.Trim()))
                {
                    MessageBox.Show("Please make sure you enter a Username and Password");
                    return;
                }

                //(1) GET the username and password from the text boxes, is good to put them in a try catch
                try
                {
                    /*YOUR CODE HERE*/
                    username = uBox.Text.Trim();
                    password = pBox.Text.Trim();
                    //using (Calendar Calendar = new Calendar())
                    //{
                    //    uBox.Text = Calendar.username;
                    //    pBox.Text = Calendar.password;
                    //}




                }

                catch (Exception ex)
                {
                    //Error message, more useful when you are storing numbers etc. into the database.
                    MessageBox.Show("Username or Password given is in an incorrect format.");
                    return;
                }

                //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
                /*YOUR CODE HERE*/

                SQL.selectQuery("SELECT * FROM CLIENT");
                //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
                /*YOUR CODE HERE*/
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                        {
                            loggedIn = true;
                            fname = SQL.read[3].ToString();
                            sname = SQL.read[2].ToString();
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("There are no accounts registed");
                    return;
                }

                //if logged in display a success message
                if (loggedIn)
                {
                    //message stating we logged in good
                    MessageBox.Show("Successfully logged in as: " + fname + " " + sname);
                    new clientCal().Show();
                    this.Hide();
                }
                else
                {
                    //message stating we couldn't log in
                    MessageBox.Show("Login attempt unsuccessful! Please check details");
                    uBox.Focus();
                    return;
                }
            }
        
            else if(adminCHK.Checked == true)
            {
                //Variables to be used: 1x bool, 4x string
                    bool loggedIn = false;
                    string username = "", password = "", fname = "", sname = "";

                //check if boxes are empty, the Trim removes white space in text from either side
                if ("".Equals(uBox.Text.Trim()) || "".Equals(pBox.Text.Trim()))
                {
                    MessageBox.Show("Please make sure you enter a Username and Password");
                    return;
                }

                //(1) GET the username and password from the text boxes, is good to put them in a try catch
                try
                {
                    /*YOUR CODE HERE*/
                    username = uBox.Text.Trim();
                    password = pBox.Text.Trim();
                }
                catch (Exception ex)
                {
                    //Error message, more useful when you are storing numbers etc. into the database.
                    MessageBox.Show("Username or Password given is in an incorrect format.");
                    return;
                }

                //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
                /*YOUR CODE HERE*/

                SQL.selectQuery("SELECT * FROM ADMINISTRATOR");
                //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
                /*YOUR CODE HERE*/
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                        {
                            loggedIn = true;
                            fname = SQL.read[3].ToString();
                            sname = SQL.read[2].ToString();
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("There are no accounts registed");
                    return;
                }

                //if logged in display a success message
                if (loggedIn)
                {
                    //message stating we logged in good
                    MessageBox.Show("Successfully logged in as: " + fname + " " + sname);
                    new adminOptions().Show();
                    this.Hide();
                }
                else
                {
                    //message stating we couldn't log in
                    MessageBox.Show("Login attempt unsuccessful! Please check details");
                    uBox.Focus();
                    return;
                }
            }
            else if(InstructCHK.Checked == true)
            {
                //Variables to be used: 1x bool, 4x string
                bool loggedIn = false;
                string username = "", password = "", fname = "", sname = "";

                //check if boxes are empty, the Trim removes white space in text from either side
                if ("".Equals(uBox.Text.Trim()) || "".Equals(pBox.Text.Trim()))
                {
                    MessageBox.Show("Please make sure you enter a Username and Password");
                    return;
                }

                //(1) GET the username and password from the text boxes, is good to put them in a try catch
                try
                {
                    /*YOUR CODE HERE*/
                    username = uBox.Text.Trim();
                    password = pBox.Text.Trim();
                }
                catch (Exception ex)
                {
                    //Error message, more useful when you are storing numbers etc. into the database.
                    MessageBox.Show("Username or Password given is in an incorrect format.");
                    return;
                }

                //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
                /*YOUR CODE HERE*/

                SQL.selectQuery("SELECT * FROM INSTRUCTOR");
                //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
                /*YOUR CODE HERE*/
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                        {
                            loggedIn = true;
                            fname = SQL.read[3].ToString();
                            sname = SQL.read[2].ToString();
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("There are no accounts registed");
                    return;
                }

                //if logged in display a success message
                if (loggedIn)
                {
                    //message stating we logged in good
                    MessageBox.Show("Successfully logged in as: " + fname + " " + sname);
                    new instructSchedule().Show();
                    this.Hide();
                }
                else
                {
                    //message stating we couldn't log in
                    MessageBox.Show("Login attempt unsuccessful! Please check details");
                    uBox.Focus();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please tick a CheckBox to validate who you are!");
            }
        }



        private void cRegoBtn_Click(object sender, EventArgs e)
        {
            new ClientReg().Show();
            this.Hide();
        }

        private void aRego_Click(object sender, EventArgs e)
        {
            new AdminReg().Show();
            this.Hide();
        }

        private void iRego_Click(object sender, EventArgs e)
        {
            new InstructReg().Show();
            this.Hide();
        }
    }
}
