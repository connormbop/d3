﻿namespace DrivingSchool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iRego = new System.Windows.Forms.Button();
            this.aRego = new System.Windows.Forms.Button();
            this.cRegoBtn = new System.Windows.Forms.Button();
            this.loginBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pBox = new System.Windows.Forms.TextBox();
            this.uBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.adminCHK = new System.Windows.Forms.RadioButton();
            this.clientCHK = new System.Windows.Forms.RadioButton();
            this.InstructCHK = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // iRego
            // 
            this.iRego.Location = new System.Drawing.Point(195, 202);
            this.iRego.Name = "iRego";
            this.iRego.Size = new System.Drawing.Size(75, 48);
            this.iRego.TabIndex = 15;
            this.iRego.Text = "Instructor Rego";
            this.iRego.UseVisualStyleBackColor = true;
            this.iRego.Click += new System.EventHandler(this.iRego_Click);
            // 
            // aRego
            // 
            this.aRego.Location = new System.Drawing.Point(104, 202);
            this.aRego.Name = "aRego";
            this.aRego.Size = new System.Drawing.Size(84, 48);
            this.aRego.TabIndex = 14;
            this.aRego.Text = "Admin Rego";
            this.aRego.UseVisualStyleBackColor = true;
            this.aRego.Click += new System.EventHandler(this.aRego_Click);
            // 
            // cRegoBtn
            // 
            this.cRegoBtn.Location = new System.Drawing.Point(15, 202);
            this.cRegoBtn.Name = "cRegoBtn";
            this.cRegoBtn.Size = new System.Drawing.Size(83, 48);
            this.cRegoBtn.TabIndex = 13;
            this.cRegoBtn.Text = "Client Rego";
            this.cRegoBtn.UseVisualStyleBackColor = true;
            this.cRegoBtn.Click += new System.EventHandler(this.cRegoBtn_Click);
            // 
            // loginBtn
            // 
            this.loginBtn.Location = new System.Drawing.Point(163, 101);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(99, 23);
            this.loginBtn.TabIndex = 12;
            this.loginBtn.Text = "Login";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Username";
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(160, 30);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(99, 20);
            this.pBox.TabIndex = 9;
            // 
            // uBox
            // 
            this.uBox.Location = new System.Drawing.Point(32, 30);
            this.uBox.Name = "uBox";
            this.uBox.Size = new System.Drawing.Size(92, 20);
            this.uBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Who are you?";
            // 
            // adminCHK
            // 
            this.adminCHK.AutoSize = true;
            this.adminCHK.Location = new System.Drawing.Point(32, 69);
            this.adminCHK.Name = "adminCHK";
            this.adminCHK.Size = new System.Drawing.Size(54, 17);
            this.adminCHK.TabIndex = 20;
            this.adminCHK.TabStop = true;
            this.adminCHK.Text = "Admin";
            this.adminCHK.UseVisualStyleBackColor = true;
            // 
            // clientCHK
            // 
            this.clientCHK.AutoSize = true;
            this.clientCHK.Location = new System.Drawing.Point(118, 69);
            this.clientCHK.Name = "clientCHK";
            this.clientCHK.Size = new System.Drawing.Size(51, 17);
            this.clientCHK.TabIndex = 21;
            this.clientCHK.TabStop = true;
            this.clientCHK.Text = "Client";
            this.clientCHK.UseVisualStyleBackColor = true;
            // 
            // InstructCHK
            // 
            this.InstructCHK.AutoSize = true;
            this.InstructCHK.Location = new System.Drawing.Point(190, 69);
            this.InstructCHK.Name = "InstructCHK";
            this.InstructCHK.Size = new System.Drawing.Size(69, 17);
            this.InstructCHK.TabIndex = 22;
            this.InstructCHK.TabStop = true;
            this.InstructCHK.Text = "Instructor";
            this.InstructCHK.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.InstructCHK);
            this.Controls.Add(this.clientCHK);
            this.Controls.Add(this.adminCHK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.iRego);
            this.Controls.Add(this.aRego);
            this.Controls.Add(this.cRegoBtn);
            this.Controls.Add(this.loginBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.uBox);
            this.Name = "Form1";
            this.Text = "Driving School";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button iRego;
        private System.Windows.Forms.Button aRego;
        private System.Windows.Forms.Button cRegoBtn;
        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox uBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton adminCHK;
        private System.Windows.Forms.RadioButton clientCHK;
        private System.Windows.Forms.RadioButton InstructCHK;
    }
}

