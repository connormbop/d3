﻿namespace DrivingSchool
{
    partial class instructSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iCalPicker = new System.Windows.Forms.DateTimePicker();
            this.bookBtn = new System.Windows.Forms.Button();
            this.iUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timeBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.booked = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // iCalPicker
            // 
            this.iCalPicker.Location = new System.Drawing.Point(23, 12);
            this.iCalPicker.Name = "iCalPicker";
            this.iCalPicker.Size = new System.Drawing.Size(324, 20);
            this.iCalPicker.TabIndex = 11;
            // 
            // bookBtn
            // 
            this.bookBtn.Location = new System.Drawing.Point(263, 377);
            this.bookBtn.Name = "bookBtn";
            this.bookBtn.Size = new System.Drawing.Size(84, 29);
            this.bookBtn.TabIndex = 10;
            this.bookBtn.Text = "Book";
            this.bookBtn.UseVisualStyleBackColor = true;
            this.bookBtn.Click += new System.EventHandler(this.bookBtn_Click);
            // 
            // iUser
            // 
            this.iUser.Location = new System.Drawing.Point(23, 118);
            this.iUser.Name = "iUser";
            this.iUser.Size = new System.Drawing.Size(149, 20);
            this.iUser.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Enter your username";
            // 
            // timeBox
            // 
            this.timeBox.FormattingEnabled = true;
            this.timeBox.Location = new System.Drawing.Point(195, 118);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(152, 21);
            this.timeBox.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Select a timeslot";
            // 
            // refreshBtn
            // 
            this.refreshBtn.Location = new System.Drawing.Point(23, 377);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(82, 29);
            this.refreshBtn.TabIndex = 17;
            this.refreshBtn.Text = "Refresh";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // booked
            // 
            this.booked.Location = new System.Drawing.Point(23, 244);
            this.booked.Name = "booked";
            this.booked.Size = new System.Drawing.Size(324, 127);
            this.booked.TabIndex = 12;
            this.booked.UseCompatibleStateImageBehavior = false;
            // 
            // instructSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 418);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.timeBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.iUser);
            this.Controls.Add(this.booked);
            this.Controls.Add(this.iCalPicker);
            this.Controls.Add(this.bookBtn);
            this.Name = "instructSchedule";
            this.Text = "instructSchedule";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker iCalPicker;
        private System.Windows.Forms.Button bookBtn;
        private System.Windows.Forms.TextBox iUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox timeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.ListView booked;
    }
}